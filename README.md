# Wedding Website

Our wedding website, built with SvelteKit and DaisyUI. Shamelessly visually adapted from [rampatra's wedding website](https://github.com/rampatra/wedding-website) with modifications. [Video](https://www.youtube.com/watch?v=JXFqkEcMkT8) in the video background section was shot by [Rhys McIntyre Videography LLC](https://www.youtube.com/channel/UCXHmqIWniKmHLdX4tBHDQnw). Uses Google Apps Script to submit email list form and RSVP form to Google Sheets. Generally the code's a bit of a mess, but it works.

Also integrated with a Notion database to toggle visibility of sections and configure dates, names, and locations.

> ## Sveltekit Typescript Tailwindcss Template
> 
> SvelteKit Starter template with Typescript, Postcss, TailwindCSS, and eslint.
> 
> Everything you need to build a Svelte project, powered by [`create-svelte`](https://github.com/sveltejs/kit/tree/master/packages/create-svelte).
> 
> ## Developing
> 
> Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:
> 
> ```bash
> npm run dev
> 
> # or start the server and open the app in a new browser tab
> npm run dev -- --open
> ```
> 
> ## Building
> 
> To create a production version of your app:
> 
> ```bash
> npm run build
> ```
> 
> You can preview the production build with `npm run preview`.
> 
> > To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.
