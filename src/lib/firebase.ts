import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import { getAuth } from 'firebase/auth';

// Initialize Firebase
const app = initializeApp({
    apiKey: 'AIzaSyCIqzyjTye5d3ackpK3XRFfNvagxcwp2PA',
    authDomain: 'wedding-website-6e274.firebaseapp.com',
    projectId: 'wedding-website-6e274',
    storageBucket: 'wedding-website-6e274.appspot.com',
    messagingSenderId: '281036784195',
    appId: '1:281036784195:web:a00d301734dd066fd44af6',
    measurementId: 'G-4TN05LN889',
});
export const db = getFirestore(app);
export const auth = getAuth(app);
