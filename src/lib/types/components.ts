export type FieldType = 'text' | 'number' | 'select' | 'password';

export type ToastType = 'success' | 'error';

export type Coordinate = {
    lat: number;
    lng: number;
};
