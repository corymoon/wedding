import type { NavItem } from '$lib/types';
import { configRepo } from '$lib/repos/config';
import { weddingIsOver } from '../utils';

class NavRepo {
    #config = configRepo.getConfig();

    #allNavItems: NavItem[] = [
        // our story
        {
            label: 'Our Story',
            href: '#our-story',
            display: this.#config.showStory && !weddingIsOver(),
        },
        // events
        {
            label: 'Events',
            href: '#events',
            display: this.#config.showEvents && !weddingIsOver(),
        },
        // photos
        {
            label: 'Photos',
            href: '#photos',
            display: this.#config.showPictures && !weddingIsOver(),
        },
        // registry
        {
            label: 'Registry',
            href: '#registry',
            display: this.#config.showRegistry && !weddingIsOver(),
        },
        {
            label: 'Wedding Day',
            href: '#weddingday',
            display: !weddingIsOver(),
        },
        {
            label: 'Venue',
            href: '#venue',
            display: this.#config.showMap && !weddingIsOver(),
        },
        {
            label: 'Accommodations',
            href: '#hotel',
            display: this.#config.showHotel && !weddingIsOver(),
        },
        {
            label: 'Dress Code',
            href: '#dress',
            display: this.#config.showDressCode && !weddingIsOver(),
        },
        {
            label: 'Thank You',
            href: '#thankyou',
            display: weddingIsOver(),
        },
    ];

    getNavItems = () => this.#allNavItems.filter((n) => n.display);
}

export const navRepo = new NavRepo();
