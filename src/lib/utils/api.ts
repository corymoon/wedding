import { signInWithEmailAndPassword, signOut } from 'firebase/auth';
import { auth, db } from '../firebase';
import {
    QueryDocumentSnapshot,
    deleteDoc,
    doc,
    updateDoc,
    addDoc,
    collection,
    getDocs,
    query,
    where,
    type DocumentData,
} from 'firebase/firestore';
import { errorToast, successToast, toastMessages } from './toast';
import { browser } from '$app/environment';
import { loading } from '../stores/loading';
import type { RsvpData } from '../types';
import { phoneFormat } from './formatters';

export const adminLogin = async (email: string, password: string) => {
    await signInWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
            console.debug(userCredential.user);
        })
        .catch((error) => {
            console.error(error.message);
        });
};

export const adminSignOut = async () => {
    await signOut(auth)
        .then(() => {
            console.debug('signed out');
        })
        .catch((error) => {
            console.error(error.message);
        });
};

export const deleteRsvp = async (id: string) => {
    await deleteDoc(doc(db, `rsvp/${id}`));
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const updateRsvp = async (rsvpId: string, docData: any) => {
    updateDoc(doc(db, `rsvp/${rsvpId}`), docData)
        .then(() => {
            successToast('Updated RSVP information');
            if (browser) localStorage.setItem('rsvp-submitted', 'true');
        })
        .catch((error) => {
            errorToast(error);
        });
};

export const rsvpDataFromQuerySnapshot = (doc: QueryDocumentSnapshot<DocumentData>) => {
    const rsvp: RsvpData = {
        id: doc.id,
        checked: doc.data().checked,
        created: doc.data().created,
        email: doc.data().email,
        guests: doc.data().guests,
        meals: doc.data().meals,
        name: doc.data().name,
        phone: doc.data().phone,
        updated: doc.data().updated,
    };
    return rsvp;
};

export const findRsvpByPhoneNumber = async (phone: string) => {
    const rsvpRef = collection(db, 'rsvp');
    const querySnapshot = await getDocs(query(rsvpRef, where('phone', '==', phoneFormat(phone))));

    if (querySnapshot.size === 1) {
        const doc = querySnapshot.docs[0];
        return rsvpDataFromQuerySnapshot(doc);
    }
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const handleRsvpSubmit = async (data: Record<string, any>) => {
    loading.set(true);
    await addDoc(collection(db, 'rsvp'), data)
        .then(() => {
            successToast(toastMessages.rsvp.success);
            if (browser) {
                localStorage.setItem('rsvp-submitted', 'true');
                localStorage.setItem('phone', phoneFormat(data.phone));
            }
            loading.set(false);
        })
        .catch((err) => {
            console.error('Error!', err.message);
            errorToast(toastMessages.rsvp.failure);
            loading.set(false);
        });
};

export const handleEmailListSubmit = async (name: string, email: string) => {
    loading.set(true);
    await addDoc(collection(db, 'emailList'), { name, email })
        .then(() => {
            successToast(toastMessages.email.success);
            loading.set(false);
        })
        .catch((err) => {
            console.error('Error!', err.message);
            errorToast(toastMessages.email.failure);
            loading.set(false);
        });
};
