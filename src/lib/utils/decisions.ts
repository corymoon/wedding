import dayjs from 'dayjs';
import { configRepo } from '../repos/config';
import type { RsvpGuest } from '../types';

export const weddingIsOver = (): boolean => daysLeft() <= 0;

export const daysLeft = (): number => {
    const weddingDay = dayjs(configRepo.getConfig().weddingDate);
    const today = dayjs();
    return weddingDay.diff(today, 'days') + 1;
};

export const rsvpSubmitIsDisabled = (
    name: string,
    email: string,
    phone: string,
    numGuests: number,
    guestMeals: RsvpGuest[],
) => {
    let arrayHasNull = false;
    for (let i = 0; i < numGuests; i++) {
        if (guestMeals[i].name == null || guestMeals[i].meal == null) {
            arrayHasNull = true;
        }
    }

    const g = numGuests < 1 || numGuests == undefined;
    if (!name || !email || !phone || g || arrayHasNull) {
        return true;
    } else {
        return false;
    }
};

export const emailListSubmitIsDisabled = (name: string, email: string) => {
    if (!name || !email) {
        return true;
    } else {
        return false;
    }
};
