// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function scrollIntoView({ target }: any): void {
    const el = document.querySelector(target.getAttribute('href'));
    if (!el) return;
    el.scrollIntoView({
        behavior: 'smooth',
    });
    target.blur();
}

export function scrollToTop() {
    window.scrollTo({ top: 0, behavior: 'smooth' });
}
