import toast from 'svelte-french-toast';
import { stringsRepo } from '../repos/strings';

export const toastMessages = stringsRepo.getToastMessages();

export const toastOptions: Record<string, unknown> = {
    duration: 8000,
    position: 'top-right',
    style: 'border-radius: 9999px; background-color: #F6F3EF;',
};

export const successIconTheme = {
    iconTheme: {
        primary: '#789ba4',
        secondary: '#F6F3EF',
    },
};

export const errorIconTheme = {
    iconTheme: {
        primary: '#d48c97',
        secondary: '#F6F3EF',
    },
};

export const successToast = (message: string) =>
    toast.success(message, { ...toastOptions, ...successIconTheme });
export const errorToast = (message: string) =>
    toast.error(message, { ...toastOptions, ...errorIconTheme });
