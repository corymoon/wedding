import { parseLocation } from 'parse-address';
import dayjs, { Dayjs } from 'dayjs';
import advancedFormat from 'dayjs/plugin/advancedFormat.js';
dayjs.extend(advancedFormat);

export const formatDate = (date: Dayjs, format: string) => date.format(format);

export const getCityState = (address: string) => {
    const parsedAddress = parseLocation(address);
    return `${parsedAddress.city}, ${parsedAddress.state}`;
};

export const venueDisplayAddress = (address: string) => address.replace(', ', '<br />');
export const venueMapAddress = (address: string) => address.replaceAll(' ', '+');

export const weddingDayString = (weddingDay: Dayjs) => dayjs(weddingDay).format('MMMM Do, YYYY');
export const rsvpDayString = (rsvpDay: Dayjs) => dayjs(rsvpDay).format("Do of MMMM 'YY");

export const phoneFormat = (input: string) => input.replace(/\D/g, '');
